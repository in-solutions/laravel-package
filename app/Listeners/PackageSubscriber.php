<?php

namespace App\Listeners;

class PackageSubscriber
{
    /**
     * Handle user login events.
     */
    public function onEntityCreated($event) {
        // project specific actions when new entity is created
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        // $events->listen(
        //     'INS\Package\EventEntityCreated',
        //     'App\Listeners\PackageSubscriber@onEntityCreated'
        // );

    }

}